/* scripts relativo al nuovo portale */
   $(document).on('keyup',function(evt) {
       if (evt.keyCode == 27 && $('body').hasClass('menu-opened')) {
         $('#chiudimenu').click();
       }
       if (evt.keyCode == 27 && $('body').hasClass('menu-opened2')) {
         $('#chiudimenu2').click();
       }       
   });

   $(document).ready(function() {
      
      if($('#tilescontainer').length > 0){
         $('#tilescontainer').imagesLoaded( function() {
            $('#tilescontainer').masonry({
               itemSelector: '.tile',
               columnWidth: '.tile'
            });
         });
      }
         
      if($('.sezioni-pannelli').length > 0){           
         $('.sezioni-pannelli').imagesLoaded( function() {
            $('.sezioni-pannelli').masonry({
               itemSelector: '.tile',
               columnWidth: '.tile'
            });
         });
      }
         $("input:checkbox").click(function() {
             if ($(this).is(":checked")) {
                 var group = "input:checkbox[name='" + $(this).attr("name") + "']";
                 $(group).prop("checked", false);
                 $(this).prop("checked", true);
             } else {
                 $(this).prop("checked", false);
             }
         });

      $('.menu-btn').click(function() {
         $('body,html').animate({scrollTop:0},200);
         $('body').toggleClass('menu-opened');
         $('.sidebar-offcanvas').toggleClass('active', 1000);
      });
      $('#chiudimenu').on('click',function(e){
         e.preventDefault();
         $('body').toggleClass('menu-opened');
         $('.sidebar-offcanvas').toggleClass('active', 1000);
      });

      $('.menu-btn-right').click(function() {
         $('body,html').animate({scrollTop:0},200);
         $('body').toggleClass('menu-opened2');
         $('.sidebar-offcanvas-right').toggleClass('active', 1000);
      });
      
      $('#chiudimenu2').on('click',function(e){
         e.preventDefault();
         $('body').toggleClass('menu-opened2');
         $('.sidebar-offcanvas-right').toggleClass('active', 1000);
      });

      
      $('.collapse').on('show.bs.collapse', function (e) {         
          $('.collapse').not(e.target).removeClass('in').prev().addClass('collapsed');
      });

      $("#topcontrol").click(function() {
         $('body,html').animate({scrollTop:0},500);
      });

      $('#key').blur(function(e){
         if($(this).val() != '' && $(this).val().length > 1){
            $(this).parentsUntil('.form').parent().submit();
         }         
      }); 
      
      /* Gallerie slider */
      if($("div[class*=' slider']").length > 0){
        $("div[class*=' slider']").each(function(i){
          $(this).slick({
            infinite: false,
            dots: false,
            centerMode: false,
            centerPadding: '6px',
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{ breakpoint: 992, settings: { slidesToShow: 2, slidesToScroll: 1, infinite: true, dots: false }  },
             { breakpoint: 768, settings: { slidesToShow: 2, slidesToScroll: 1, dots: false } },
             { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1, dots: false } },
             { breakpoint: 360, settings: { slidesToShow: 1, slidesToScroll: 1, dots: false } }
             ]
          });
        });
      }

      /* Agenda slider */
      if($("div[class*='agendaslider']").length > 0){
         
        $("div[class*='agendaslider']").each(function(i){
          $(this).slick({
            infinite: false,
            dots: false,
            centerMode: false,
            centerPadding: '6px',
            initialSlide: 4,
            slidesToShow: 12,
            slidesToScroll: 6,
              responsive: [
                { breakpoint: 992, settings: { centerPadding: '6px', slidesToShow: 9, slidesToScroll: 6, dots: false }},
                { breakpoint: 768, settings: { centerPadding: '6px', slidesToShow: 6,  slidesToScroll: 3, dots: false }},
                { breakpoint: 480, settings: { centerPadding: '6px', slidesToShow: 3,  slidesToScroll: 1, dots: false }}                
              ]             
          });
        });
      }
      
      $.cookiesDirective({
      privacyPolicyUri: '#',// uri of your privacy policy
      privacyPolicyTitle: 'Informativa cookies',// uri of your privacy policy
      explicitConsent: false, // true // false allows implied consent
      position: 'bottom', // top or bottom of viewport
      duration: 100, // display time in seconds
      limit: 0, // limit disclosure appearances, 0 is forever    
      message: "Questo sito utilizza i cookies per migliorare l'esperienza di navigazione. Continuando la navigazione accetti l'utilizzo dei cookies. ", // customise the disclosure message             
      cookieScripts: null, // disclose cookie settings scripts
      scriptWrapper: function(){}, // wrapper function for cookie setting scripts
      fontFamily: 'helvetica', // font style for disclosure panel
      fontColor: '#FFFFFF', // font color for disclosure panel
      fontSize: '13px', // font size for disclosure panel
      backgroundColor: '#000000', // background color of disclosure panel
      backgroundOpacity: '85', // opacity of disclosure panel
      linkColor: '#ffffff' // link color in disclosure panel
      });

   $("#feedbackform").submit(function(){
         $('#messages').html('');
         $missing = false;
         $error = false;
         $('input,textarea,select').filter('[required]:visible').each(function(){
               if($(this).val() == ''){
                  $(this).addClass('missing');
                  $missing = true;
               }               
               if(!validateEmail($('#email').val())){
                     $('#email').addClass('errorField');
                     $error = true;
               }
         });
         if($missing){
            $('#messages').html('<div class="alert alert-warning" role="alert">  Attenzione controlla attentamente i campi </div>');
         }
         if($error){
          $('#messages').html('<div role="alert" class="alert alert-danger">   Attenzione controlla attentamente i campi </div>');
         }
          
          if(!$error && !$missing){
             return true;
         }
         else{
            return false;
         }
         
   });

   });
   
   $(window).load(function(){
      jQuery('.sezioni-pannelli').resize(); 
   });
   
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  $("#result").text("");
  var email = $("#email").val();
  if (validateEmail(email)) {
    $("#result").text(email + " is valid :)");
    $("#result").css("color", "green");
  } else {
    $("#result").text(email + " is not valid :(");
    $("#result").css("color", "red");
  }
  return false;
}

   
$(window).on("scroll touchmove", function () {
  $('#mainheader').toggleClass('ridotto', $(document).scrollTop() > 32);
  $('body').toggleClass('ridotto', $(document).scrollTop() > 32);
  $('#topcontrol').toggleClass('visualizza', $(document).scrollTop() > 200);
});
