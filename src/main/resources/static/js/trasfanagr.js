function showAlert(type, text){

  $('#container-alert').append(`<div id="error-alert" tabindex='1' class="Prose Alert Alert--error Alert--withIcon u-layout-prose u-padding-r-bottom u-padding-r-right u-margin-r-bottom " role="alert"  >
                <h2 class="u-text-h3">Si è verificato un errore
                </h2>
                <p class="u-text-p">${text}</p>
                <br>
             </div>`);

  $('#error-alert').focus();
}