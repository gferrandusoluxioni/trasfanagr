package it.soluxioni.trasfanagr.services;

import it.soluxioni.trasfanagr.model.Caricamento;
import it.soluxioni.trasfanagr.model.Istituzione;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.repositories.CaricamentoRepository;
import it.soluxioni.trasfanagr.repositories.IstituzioneRepository;
import it.soluxioni.trasfanagr.repositories.UtenteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CaricamentoServiceImpl implements CaricamentoService {

    private CaricamentoRepository caricamentoRepository;
    private IstituzioneRepository istituzioneRepository;

    private UtenteRepository utenteRepository;

    public CaricamentoServiceImpl(CaricamentoRepository caricamentoRepository, IstituzioneRepository istituzioneRepository, UtenteRepository utenteRepository) {
        this.caricamentoRepository = caricamentoRepository;
        this.istituzioneRepository = istituzioneRepository;

        this.utenteRepository = utenteRepository;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Caricamento salvaNuovoCaricamento(MultipartFile[] files, String directoryName, String[] estensioni, String istituzioneCaricata, String tipoRilevazione, String anno, boolean isMultiple, Utente utente, String note) throws IOException {

        String[] filePaths = new String[8];

        Istituzione istituzione = istituzioneRepository.findByCodice(istituzioneCaricata);
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        String month = String.valueOf(Calendar.getInstance().get(Calendar.MONTH));
        String day = String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String time = sdf.format(Calendar.getInstance().getTime());

        for(int i = 0; i < filePaths.length; i++){
            if(estensioni[i] != null && !estensioni[i].equals(""))
                filePaths[i] = directoryName+"/"+ istituzione.getCodice()+"_"+year+"_"+month+"_"+day+"__"+time+"_file"+String.valueOf(i+1)+"."+estensioni[i];
        }

        Caricamento caricamento = new Caricamento();
        caricamento.setDataCaricamento(new Date());
        caricamento.setTipoPrelievo(tipoRilevazione);
        caricamento.setAnnoScolastico(anno);
        caricamento.setMultiFileSiNo(isMultiple);
        caricamento.setUtenteCaricante(utente);
        caricamento.setIstituzioneCaricata(istituzione.getDenominazione());
        caricamento.setProvinciaCaricante(istituzione.getProvincia());
        caricamento.setComuneCaricante(istituzione.getNomeComune());

        caricamento.setNote(note);

        for(int i=0; i< files.length; i++){
            if(filePaths[i] != null){
                files[i].transferTo(new File(filePaths[i]));
            }
        }

        caricamento.setUpload1(filePaths[0]);
        caricamento.setUpload2(filePaths[1]);
        caricamento.setUpload3(filePaths[2]);
        caricamento.setUpload4(filePaths[3]);
        caricamento.setUpload5(filePaths[4]);
        caricamento.setUpload6(filePaths[5]);
        caricamento.setUpload7(filePaths[6]);
        caricamento.setUpload8(filePaths[7]);

        caricamento = caricamentoRepository.save(caricamento);
        utente.getCaricamenti().add(caricamento);
        for(Istituzione isti : utente.getIstituzioni()){
            isti.setResponsabile(utente);
            istituzioneRepository.save(isti);
        }

        utenteRepository.save(utente);

        return caricamentoRepository.save(caricamento);
    }

    @Override
    public boolean controllaEstensioni(String[] estensioni, boolean[] estensioniApprovate) {

        boolean tutteEstensioniOk = true;

        for(int i=0; i<estensioniApprovate.length; i++){
            if(!estensioniApprovate[i]){
                tutteEstensioniOk = false;
                return tutteEstensioniOk;
            }
        }
        return tutteEstensioniOk;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public List<Caricamento> prendiCaricamentiDi(String username) {

        return caricamentoRepository.findAllByUtenteCaricante(username);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Optional<Caricamento> prendiCaricamentoPerID(Long id) {
        return caricamentoRepository.findById(id);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public List<Caricamento> prendiTutti() {
        return caricamentoRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void eliminaCaricamento(Caricamento caricamento) {
        caricamentoRepository.delete(caricamento);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void eliminaCaricamentoDaDisco(Long id) throws IOException{
        Optional<Caricamento> optionalCaricamento = prendiCaricamentoPerID(id);
        Caricamento caricamento = optionalCaricamento.get();
        Utente utente = caricamento.getUtenteCaricante();
        caricamento.setUtenteCaricante(null);

        if(caricamento.getUpload1() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload1());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload2() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload2());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload3() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload3());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload4() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload4());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload5() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload5());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload6() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload6());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload7() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload7());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload8() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload8());
            Files.delete(fileToDeletePath);
        }
    }
}
