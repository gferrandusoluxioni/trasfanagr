package it.soluxioni.trasfanagr.services;

import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.repositories.UtenteRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Primary
public class UserDetailServiceImpl implements UserDetailsService {

    private UtenteRepository utenteRepository;

    public UserDetailServiceImpl(UtenteRepository utenteRepository) {
        this.utenteRepository = utenteRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Utente utente = utenteRepository.findByUsername(username);
        if(utente == null)
            return null;

        String password = utente.getPassword();
        boolean active = true; //TODO
        String ruolo = utente.getRuolo();
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        switch (ruolo) {
            case "REGISTRATO": grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_REGISTRATO"));
                                break;
        /*    case "PRIMOACCESSO":  grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_REGISTRATO"));
                                  grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_PRIMOACCESSO"));
                                  break;
            case "DOCUMENTATO": grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_REGISTRATO"));
                                grantedAuthorities.add(new SimpleGrantedAuthority("DOCUMENTATO"));
                                break;*/
            case "ABILITATO": grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_REGISTRATO"));
                                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ABILITATO"));
                                break;
            case "PROVINCIA":  grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_REGISTRATO"));
                                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ABILITATO"));
                                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_PROVINCIA"));
                                break;
            case "ADMIN":       grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_REGISTRATO"));
                                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ABILITATO"));
                                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_PROVINCIA"));
                                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }

        User myUser = new User(username, password, active, active, active, active, grantedAuthorities);
        return myUser;
    }


}
