package it.soluxioni.trasfanagr.services;

import javax.mail.MessagingException;

public interface MailService {

    void inviaMail(String oggetto, String testo, String indirizzo) throws MessagingException;
}
