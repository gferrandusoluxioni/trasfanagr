package it.soluxioni.trasfanagr.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.soluxioni.trasfanagr.model.Istituzione;
import it.soluxioni.trasfanagr.model.TokenRegistrazione;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.repositories.IstituzioneRepository;
import it.soluxioni.trasfanagr.repositories.TokenRegistrazioneRepository;
import it.soluxioni.trasfanagr.repositories.UtenteRepository;
import it.soluxioni.trasfanagr.utility.Utility;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class UtenteServiceImpl implements UtenteService {

    private UtenteRepository utenteRepository;
    private IstituzioneRepository istituzioneRepository;
    private TokenRegistrazioneRepository tokenRegistrazioneRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UtenteServiceImpl(UtenteRepository utenteRepository, IstituzioneRepository istituzioneRepository, TokenRegistrazioneRepository tokenRegistrazioneRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.utenteRepository = utenteRepository;
        this.istituzioneRepository = istituzioneRepository;
        this.tokenRegistrazioneRepository = tokenRegistrazioneRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Utente registraNuovoAccount(final String user) throws IOException, ParseException {

        String jsonUser = user;

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(jsonUser);
        Utente nuovoUtente = new Utente();
        nuovoUtente.setCodiceFiscale(jsonNode.get("codiceFiscale").toString().replace("\"", ""));
        nuovoUtente.setNome(jsonNode.get("nome").toString().replace("\"", ""));
        nuovoUtente.setCognome(jsonNode.get("cognome").toString().replace("\"", ""));

        String data =  jsonNode.get("data").toString().replace("\"", "");
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN);
        Date date = format.parse(data);
        nuovoUtente.setCompleanno(date);
        nuovoUtente.setNazione(jsonNode.get("nazione").toString().replace("\"", ""));
        nuovoUtente.setRegione(jsonNode.get("regione").toString().replace("\"", ""));
        nuovoUtente.setProvincia(jsonNode.get("provincia").toString().replace("\"", ""));
        nuovoUtente.setCodiceComune(jsonNode.get("codiceComune").toString().replace("\"", ""));
        nuovoUtente.setComune(jsonNode.get("comune").toString().replace("\"", ""));
        boolean maschioSiNo = (jsonNode.get("sex").toString().replace("\"", "")).equals("M")  ? true : false;
        nuovoUtente.setMaschioSiNo(maschioSiNo);
        nuovoUtente.setEmail(jsonNode.get("email").toString().replace("\"", ""));

        JsonNode codesNode = jsonNode.get("istituzioni");
        Set<String> codici = dammiCodiciIstituzioni(codesNode);
        Set<Istituzione> istituzioni = dammiIstituzioni(codici);

        List<Istituzione> istituzioniAssociate = new ArrayList<>(istituzioni);
        nuovoUtente.setProvinciaAssociata(istituzioniAssociate.get(0).getProvincia());

        nuovoUtente.setIstituzioni(istituzioni);

        String nome = nuovoUtente.getNome().replaceAll("\\s","").replaceAll("'", "");
        String cognome = nuovoUtente.getCognome().replaceAll("\\s","").replaceAll("'", "");

        String username = generaUsername(nome, cognome);
        nuovoUtente.setUsername(username);
        nuovoUtente.setRuolo("REGISTRATO");
        nuovoUtente.setDataUltimaModificaAccount(new Date());
        nuovoUtente.setPrimaPasswordSiNo(true);
        nuovoUtente.setTipoUtente("ISTITUZIONESCOLASTICA");

        Utente registrato = utenteRepository.save(nuovoUtente);

        for(Istituzione istituzione : nuovoUtente.getIstituzioni()){
            istituzione.setResponsabile(registrato);
            istituzioneRepository.save(istituzione);
        }
        return registrato;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void creaTokenRegistrazione(Utente utente, String token) {
        TokenRegistrazione myToken = new TokenRegistrazione(token, utente);
        tokenRegistrazioneRepository.save(myToken);
    }

    @Override
    public TokenRegistrazione getTokenRegistrazione(String token) {
        return tokenRegistrazioneRepository.findByToken(token);
    }

    @Override
    public Utente getUtente(String token) {
        Utente user = tokenRegistrazioneRepository.findByToken(token).getUser();
        return user;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public String salvaUtenteRegistrato(Utente user) {
        String password = generaPassword();
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setDataUltimaModificaAccount(new Date());
        utenteRepository.save(user);
        return password;

    }


    @Override
    @Transactional(rollbackFor=Exception.class)
    public Utente salvaDocumentiUtente(String username, String idPath, String cfPath) {
        Utente utente = utenteRepository.findByUsername(username);
        if(utente == null)
            return null;

        utente.setDocumentoCartaId(idPath);
        utente.setCIdSiNo(true);
        utente.setDocumentoCodFisc(cfPath);
        utente.setCfSiNo(true);
        utente.setDataUltimaModificaAccount(new Date());
        return utenteRepository.save(utente);
    }

    private boolean esisteQuestoUsername(String username){
        Utente utente = utenteRepository.findByUsername(username);
        if (utente == null) {
            return false;
        }
        return true;
    }

    private String generaUsername(String nome, String cognome){

        String defaultUsername= nome+"."+cognome;
        int index = 1;

        if(esisteQuestoUsername(defaultUsername)){
            while(esisteQuestoUsername(defaultUsername)){
                defaultUsername += index;
            }
            return defaultUsername;
        } else {
            return defaultUsername;
        }
    }

    private Set<String> dammiCodiciIstituzioni(JsonNode codesNode){

        Set<String> codiciIstituzioni = new HashSet<>();
        for(int i = 0; i < codesNode.size(); i++){
            codiciIstituzioni.add(codesNode.get(i).toString().replace("\"", ""));
        }
        return codiciIstituzioni;
    }


    private Set<Istituzione> dammiIstituzioni(Set<String> codici){
        Set<Istituzione> istituzioni = new HashSet<>();

        for(String codice : codici) {
           Istituzione istituzione = istituzioneRepository.findByCodice(codice);
           istituzioni.add(istituzione);
        }
        return istituzioni;
    }

    @Override
    public Utente getUtenteByUsername(String username) {
        return utenteRepository.findByUsername(username);
    }

    @Override
    public Utente getUtenteByCodiceFiscale(String codicefiscale) {
        return utenteRepository.findByCodiceFiscale(codicefiscale);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public String cambiaPassword(String jsonData, String username) throws IOException {

        Utente utente = utenteRepository.findByUsername(username);
        if(utente == null)
            return "INESISTENTE";

        String data = jsonData;

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(data);
        String vecchiaPassword = jsonNode.get("old").toString().replace("\"", "");
        if(!bCryptPasswordEncoder.matches(vecchiaPassword, utente.getPassword()))
            return "VECCHIA";

        String nuovaPassword = jsonNode.get("new").toString().replace("\"", "");
        String confermaPassword = jsonNode.get("conf").toString().replace("\"", "");

        if(nuovaPassword.equals(confermaPassword)){
            if(Utility.checkCriteriPassword(nuovaPassword)){

               if(!vecchiaPassword.equals(nuovaPassword)) {
                utente.setPassword(bCryptPasswordEncoder.encode(nuovaPassword));
                utente.setPrimaPasswordSiNo(false);
                utente.setDataUltimaModificaAccount(new Date());
                utenteRepository.save(utente);
                return "OK";
               } else {
                   return "UGUALI";
               }

            } else {
                return "CRITERI";
            }
        }

        return "NO_MATCH";
    }

    @Override
    public List<Utente> tuttiUtenti() {
        return utenteRepository.findAllByEliminatoSiNoFalse();
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Utente aggiornaUtente(Utente utente) {
        utente.setDataUltimaModificaAccount(new Date());
        return utenteRepository.save(utente);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public String rigeneraPasswordDimenticata(String username) {

        Utente utente = utenteRepository.findByUsername(username);
        if (utente == null)
            return null;

        String nuovaPassword = generaPassword();
        utente.setPassword(bCryptPasswordEncoder.encode(nuovaPassword));
        utente.setPrimaPasswordSiNo(true);
        utente.setDataUltimaModificaAccount(new Date());
        utenteRepository.save(utente);
        return nuovaPassword;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void eliminaUtente(String username) throws IOException {

        Utente utente = utenteRepository.findByUsername(username);
        Set<Istituzione> istituzioni = utente.getIstituzioni();
        for(Istituzione istituzione : istituzioni){
            istituzione.setResponsabile(null);
            istituzioneRepository.save(istituzione);
        }
        TokenRegistrazione token = tokenRegistrazioneRepository.findByUser(utente);
        token.setUser(null);
        tokenRegistrazioneRepository.delete(token);
        utenteRepository.delete(utente);
    }

    private String generaPassword(){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();
        return generatedString;
    }

    @Override
    public Utente getUtenteByEmail(String email) {
        return utenteRepository.findByEmail(email);
    }
}
