package it.soluxioni.trasfanagr.services;

import it.soluxioni.trasfanagr.utility.Costanti;
import it.soluxioni.trasfanagr.utility.MessaggiEmail;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailServiceImpl implements MailService {

    private JavaMailSender javaMailSender;

    public MailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void inviaMail(String oggetto, String testo, String indirizzo) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        String htmlMsg = MessaggiEmail.inviaMail(testo);
        mimeMessage.setContent(htmlMsg, "text/html");
        helper.setTo(indirizzo);
        helper.setSubject(oggetto);
        helper.setFrom(Costanti.APP_EMAIL);
        javaMailSender.send(mimeMessage);
    }
}
