package it.soluxioni.trasfanagr.services;

import it.soluxioni.trasfanagr.model.Caricamento;
import it.soluxioni.trasfanagr.model.Utente;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface CaricamentoService {
    Caricamento salvaNuovoCaricamento(MultipartFile[] files, String directoryName, String[] estensioni, String istituzioneCaricata, String tipoRilevazione, String anno, boolean isMultiple, Utente utente, String note) throws IOException;
    boolean controllaEstensioni(String[] estensioni, boolean[] estensioniApprovate);
    List<Caricamento> prendiCaricamentiDi(String username);
    Optional<Caricamento> prendiCaricamentoPerID(Long id);
    List<Caricamento> prendiTutti();
    void eliminaCaricamento(Caricamento caricamento) throws IOException;
    void eliminaCaricamentoDaDisco(Long id) throws IOException;
}
