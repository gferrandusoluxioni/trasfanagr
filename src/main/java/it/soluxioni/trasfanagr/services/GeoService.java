package it.soluxioni.trasfanagr.services;

import it.soluxioni.trasfanagr.model.Comune;

import java.util.List;
import java.util.Map;

public interface GeoService {

    Map<String, String> getRegionitaliane();
    Map<String, String> getProvince(String regione);
    Map<String, String> getComuni(String provincia);
    Map<String, String> getNazioni();
    List<String> getCodiciCatastaliByProvincia(String provincia);
    List<Comune> getComuniByRegione(String nomeRegione);
}
