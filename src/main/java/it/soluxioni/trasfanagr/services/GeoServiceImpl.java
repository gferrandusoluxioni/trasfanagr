package it.soluxioni.trasfanagr.services;

import it.soluxioni.trasfanagr.model.Comune;
import it.soluxioni.trasfanagr.repositories.ComuneRepository;
import it.soluxioni.trasfanagr.repositories.NazioneRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GeoServiceImpl implements GeoService {

    private ComuneRepository comuneRepository;
    private NazioneRepository nazioneRepository;

    public GeoServiceImpl(ComuneRepository comuneRepository, NazioneRepository nazioneRepository) {
        this.comuneRepository = comuneRepository;
        this.nazioneRepository = nazioneRepository;
    }

    @Override
    public Map<String, String> getNazioni() {
        List<Object[]>  idENazioni =  nazioneRepository.findAllNazioni();
        return getCodiciENomi(idENazioni);
    }

    @Override
    public Map<String, String> getRegionitaliane() {
        List<Object[]>  codiciRegioni =  comuneRepository.findAllRegioni();
        return getCodiciENomi(codiciRegioni);
    }

    @Override
    public Map<String, String> getProvince(String regione) {
        List<Object[]> province = comuneRepository.findProvinceByCodiceRegione(regione);
        return getCodiciENomi(province);
    }

    @Override
    public Map<String, String> getComuni(String provincia) {

        List<Comune> comuni = comuneRepository.findByCodiceProvincia(provincia);
        Map<String, String> codiciNomiComuni = new HashMap<>();
        for (Comune comune: comuni) {
            codiciNomiComuni.put(comune.getCodiceComune(), comune.getNomeComune());
        }
        return codiciNomiComuni;
    }

    private Map<String, String> getCodiciENomi(List<Object[]> lista) {
        Map<String, String> codiciNomi = new HashMap<>();

        for (int i = 0; i < lista.size(); i++) {
            Object[] row = (Object[]) lista.get(i);
            List<String> codEnome = Arrays.asList((Arrays.toString(row)).split(","));
            String codice = codEnome.get(0).replace("[", "");
            String nome = codEnome.get(1).replace("]", "");
            codiciNomi.put(codice, nome);
        }
        return codiciNomi;
    }

    @Override
    public List<String> getCodiciCatastaliByProvincia(String provincia) {
        return comuneRepository.findCodiciCatastaliProvincia(provincia);
    }

    @Override
    public List<Comune> getComuniByRegione(String nomeRegione) {
        return comuneRepository.findByNomeRegione(nomeRegione);
    }
}
