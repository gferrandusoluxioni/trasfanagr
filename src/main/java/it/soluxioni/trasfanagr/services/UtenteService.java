package it.soluxioni.trasfanagr.services;

import it.soluxioni.trasfanagr.model.TokenRegistrazione;
import it.soluxioni.trasfanagr.model.Utente;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface UtenteService {

    Utente registraNuovoAccount (String jsonAccount) throws IOException, ParseException;   //todo throws UserAlreadyExistException;

    void creaTokenRegistrazione (Utente utente, String token);

    TokenRegistrazione getTokenRegistrazione(String token);

    Utente getUtente(String token);

    String salvaUtenteRegistrato(Utente user);

    Utente salvaDocumentiUtente(String username, String idPath, String cfPath);

    Utente getUtenteByUsername(String username);

    Utente getUtenteByCodiceFiscale(String codiceFiscale);

    String cambiaPassword(String jsonData, String username) throws IOException;

    List<Utente> tuttiUtenti();

    Utente aggiornaUtente(Utente utente);

    String rigeneraPasswordDimenticata(String username);

    void eliminaUtente(String username) throws IOException;

    Utente getUtenteByEmail(String email);

}
