package it.soluxioni.trasfanagr.events;

import it.soluxioni.trasfanagr.model.Utente;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnPasswordDimenticataEvent extends ApplicationEvent {

    private Utente utente;
    private String password;

    public OnPasswordDimenticataEvent(Object source, Utente utente, String password) {
        super(source);
        this.utente = utente;
        this.password = password;
    }
}
