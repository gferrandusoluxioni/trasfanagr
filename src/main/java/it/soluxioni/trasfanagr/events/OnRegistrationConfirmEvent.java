package it.soluxioni.trasfanagr.events;

import it.soluxioni.trasfanagr.model.Utente;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletRequest;

@Getter
@Setter
public class OnRegistrationConfirmEvent extends ApplicationEvent {

    private Utente utente;
    private String passwordProvvisoria;
    private HttpServletRequest request;

    public OnRegistrationConfirmEvent(Object source, Utente utente, String passwordProvvisoria, HttpServletRequest request) {
        super(source);
        this.utente = utente;
        this.passwordProvvisoria = passwordProvvisoria;
    }
}
