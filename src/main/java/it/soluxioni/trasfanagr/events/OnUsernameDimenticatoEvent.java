package it.soluxioni.trasfanagr.events;

import it.soluxioni.trasfanagr.model.Utente;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnUsernameDimenticatoEvent extends ApplicationEvent {
    private Utente utente;

    public OnUsernameDimenticatoEvent(Object source, Utente utente) {
        super(source);
        this.utente = utente;
    }
}
