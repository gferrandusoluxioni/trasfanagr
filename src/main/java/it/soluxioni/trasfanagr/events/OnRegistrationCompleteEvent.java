package it.soluxioni.trasfanagr.events;

import it.soluxioni.trasfanagr.model.Utente;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletRequest;

@Getter
@Setter
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private Utente utente;
    private HttpServletRequest request;

    public OnRegistrationCompleteEvent(Object source, Utente utente, HttpServletRequest request) {
        super(source);
        this.request = request;
        this.utente = utente;
    }
}
