package it.soluxioni.trasfanagr.repositories;

import it.soluxioni.trasfanagr.model.Caricamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CaricamentoRepository extends JpaRepository<Caricamento, Long> {

    List<Caricamento> findAllByUtenteCaricante(String username);
}
