package it.soluxioni.trasfanagr.repositories;

import it.soluxioni.trasfanagr.model.Istituzione;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface IstituzioneRepository extends JpaRepository<Istituzione, Long> {

    List<Istituzione> findAllByProvincia(String provincia);


    Istituzione findByCodice(String codice);


    @Query("SELECT codice FROM Istituzione WHERE responsabile IS NULL")
    Set<String> findAllCodes();

    List<Istituzione> findByResponsabileIsNotNull();
}
