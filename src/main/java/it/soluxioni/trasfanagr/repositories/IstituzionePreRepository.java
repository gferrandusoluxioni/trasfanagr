/*
package it.soluxioni.trasfanagr.repositories;

import it.soluxioni.trasfanagr.model.IstituzionePre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface IstituzionePreRepository extends JpaRepository<IstituzionePre, Long> {

    List<IstituzionePre> findAllByProvincia(String provincia);


    IstituzionePre findByCodice(String codice);

    @Query("SELECT codice FROM IstituzionePre ")
    Set<String> findAllCodes();
}
*/
