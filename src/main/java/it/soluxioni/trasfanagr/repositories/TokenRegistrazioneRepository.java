package it.soluxioni.trasfanagr.repositories;

import it.soluxioni.trasfanagr.model.TokenRegistrazione;
import it.soluxioni.trasfanagr.model.Utente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRegistrazioneRepository extends JpaRepository<TokenRegistrazione, Long> {

    TokenRegistrazione findByToken(String token);
    TokenRegistrazione findByUser(Utente user);


}
