package it.soluxioni.trasfanagr.repositories;


import it.soluxioni.trasfanagr.model.Comune;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComuneRepository extends JpaRepository<Comune, Long> {

    List<Comune> findByNomeRegione(String nomeRegione);

    @Query("SELECT DISTINCT codiceProvincia, nomeProvincia FROM Comune WHERE codiceRegione = ?1")
    List<Object[]> findProvinceByCodiceRegione(String codiceRegione);

    @Query("SELECT DISTINCT codiceRegione, nomeRegione FROM Comune")
    List<Object[]> findAllRegioni();

    List<Comune> findByCodiceProvincia(String codProvincia);

    @Query("SELECT DISTINCT codiceCatasto FROM Comune WHERE nomeProvincia =?1")
    List<String> findCodiciCatastaliProvincia(String provincia);

}
