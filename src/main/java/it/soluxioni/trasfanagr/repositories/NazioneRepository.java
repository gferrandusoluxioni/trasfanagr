package it.soluxioni.trasfanagr.repositories;

import it.soluxioni.trasfanagr.model.Nazione;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NazioneRepository extends JpaRepository<Nazione, Long> {

    @Query("SELECT id, nome FROM Nazione")
    List<Object[]> findAllNazioni();
}
