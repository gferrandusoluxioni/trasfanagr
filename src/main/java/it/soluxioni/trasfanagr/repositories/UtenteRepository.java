package it.soluxioni.trasfanagr.repositories;

import it.soluxioni.trasfanagr.model.Utente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UtenteRepository extends JpaRepository<Utente, Long> {

    Utente findByUsername(String username);
    Utente findByCodiceFiscale(String codiceFiscale);
    Utente findByEmail(String email);
    List<Utente> findAllByEliminatoSiNoFalse();
}
