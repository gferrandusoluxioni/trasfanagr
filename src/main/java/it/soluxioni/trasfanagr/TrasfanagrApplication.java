package it.soluxioni.trasfanagr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrasfanagrApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrasfanagrApplication.class, args);

    }

}
