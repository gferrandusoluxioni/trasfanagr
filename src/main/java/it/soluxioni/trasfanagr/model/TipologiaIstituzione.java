package it.soluxioni.trasfanagr.model;

public enum TipologiaIstituzione {
    STATALE,
    PARITARIA
}
