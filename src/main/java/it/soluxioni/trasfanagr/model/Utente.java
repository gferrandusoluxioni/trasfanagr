package it.soluxioni.trasfanagr.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "utenti")
@Getter
@Setter
public class Utente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;
    private String password;

    private String nome;
    private String cognome;

    private Date compleanno;
    private String nazione;
    private String regione;
    private String provincia;

    // ???
    private String tipoUtente;      //annalisa
    private String codiceUtente;

    private String comune;
    private String codiceComune;

    private boolean maschioSiNo;

    // ???
    //@Enumerated(value = EnumType.STRING)
    private String ruolo;
    @Column(nullable = false, unique = true)
    private String codiceFiscale;
    @Column(nullable = false, unique = true)
    private String email;

    private boolean cIdSiNo;
    private String documentoCartaId;
    private boolean cfSiNo;
    private String documentoCodFisc;

    private String provinciaAssociata;


    private boolean abilitatoSiNo;
    private boolean primaPasswordSiNo;

    //private Date timeLink;                          //se l'utente non ha modificato la pswd dopo un'ora da questo tempo il link per email scade

    private String utenteUltimaModificaAccount;     //nome dell'utente che ha modificato l'account collegato a questo utente
    private Date dataUltimaModificaAccount;         //ogni volta che cambio la password aggiorno questo

    @OneToMany(mappedBy = "responsabile", fetch = FetchType.EAGER)
    private Set<Istituzione> istituzioni = new HashSet<>();

    @OneToMany(mappedBy = "utenteCaricante", fetch = FetchType.EAGER)
    private List<Caricamento> caricamenti = new ArrayList<>();

    private boolean eliminatoSiNo;

    private Date scadenzaPassword;


    public Utente() {
        primaPasswordSiNo = true;
        eliminatoSiNo = false;
    }
}
