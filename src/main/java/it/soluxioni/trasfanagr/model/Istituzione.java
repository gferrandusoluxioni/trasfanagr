package it.soluxioni.trasfanagr.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Table(name = "istituzioni")
@Getter
@Setter
public class Istituzione {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String codice;
    private String provincia;
    private String denominazione;
    private String indirizzo;
    private String cap;
    private String codiceComune;
    private String nomeComune;
    private String descrizioneTipologiaGradoIstruzione;
    private String email;
    private String pec;
    private String sitoWeb;
    @Enumerated(value = EnumType.STRING)
    private TipologiaIstituzione tipologia;
    @ManyToOne
    private Utente responsabile;
    /*@ManyToOne
    private Caricamento caricamento;*/

}
