package it.soluxioni.trasfanagr.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "caricamenti")
@Getter
@Setter
public class Caricamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date dataCaricamento;

    // ???
    //@Enumerated(value = EnumType.STRING)
    private String tipoPrelievo;
    private String annoScolastico;


    private boolean multiFileSiNo;

    // ??? mah
    /*@OneToMany(mappedBy = "path", fetch = FetchType.EAGER)
    private Set<File> files = new HashSet<>();*/
    private String upload1;
    private String upload2;
    private String upload3;
    private String upload4;
    private String upload5;
    private String upload6;
    private String upload7;
    private String upload8;

    // ???
    /*    @OneToMany(mappedBy = "caricamento")
    private Set<Istituzione> istituzioniCaricanti = new HashSet<>();*/

    @ManyToOne
    private Utente utenteCaricante;

    // ???
    private String istituzioneCaricata;
    private String provinciaCaricante;
    private String comuneCaricante;

    @Size(max = 1000)
    private String note;
}
