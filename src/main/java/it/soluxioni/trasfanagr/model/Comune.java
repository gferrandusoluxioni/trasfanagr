package it.soluxioni.trasfanagr.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "comuni")
public class Comune {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nomeComune;
    private String codiceComune;

    private String codiceCatasto;

    private String anno;

    //  ???
    private String nomeRegione;
    private String codiceRegione;
    private String nomeProvincia;
    private String codiceProvincia;
}
