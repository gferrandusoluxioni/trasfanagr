package it.soluxioni.trasfanagr.listeners;

import it.soluxioni.trasfanagr.events.OnUtenteAbilitatoEvent;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.utility.MessaggiEmail;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class UtenteAbilitatoListener implements ApplicationListener<OnUtenteAbilitatoEvent> {

    private JavaMailSender javaMailSender;

    public UtenteAbilitatoListener(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnUtenteAbilitatoEvent onUtenteAbilitatoEvent) {
        try{
            this.notificaAbilitazione(onUtenteAbilitatoEvent);
        }catch (MessagingException e) {e.printStackTrace();}
    }

    private void notificaAbilitazione(OnUtenteAbilitatoEvent event) throws MessagingException {

        Utente utente = event.getUtente();

        String email = utente.getEmail();
        String username = utente.getUsername();

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        String htmlMsg = MessaggiEmail.creaUtenteAbiliatoMail(username);
        mimeMessage.setContent(htmlMsg, "text/html");
        helper.setTo(email);
        helper.setSubject("Sistema Informativo Anagrafe Regionale degli Studenti - Account abilitato");
        helper.setFrom("dati-studenti@regione.toscana.it");
        javaMailSender.send(mimeMessage);
    }
}
