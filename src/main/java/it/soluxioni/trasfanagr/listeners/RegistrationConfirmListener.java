package it.soluxioni.trasfanagr.listeners;

import it.soluxioni.trasfanagr.events.OnRegistrationConfirmEvent;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.utility.MessaggiEmail;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class RegistrationConfirmListener implements ApplicationListener<OnRegistrationConfirmEvent> {

    private JavaMailSender javaMailSender;

    public RegistrationConfirmListener(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationConfirmEvent onRegistrationConfirmEvent) {
        try{
            this.inviaCredenziali(onRegistrationConfirmEvent);
        } catch (MessagingException e) {e.printStackTrace();}
    }

    private void inviaCredenziali(OnRegistrationConfirmEvent event) throws MessagingException {
        Utente utente = event.getUtente();
        String username = utente.getUsername();
        String password = event.getPasswordProvvisoria();
        String email = utente.getEmail();

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        String htmlMsg = MessaggiEmail.confermaRegistrazioneMail(utente.getUsername(), password);
        mimeMessage.setContent(htmlMsg, "text/html");
        helper.setTo(email);
        helper.setSubject("Sistema Informativo Anagrafe Regionale degli Studenti - Account attivo");
        helper.setFrom("dati-studenti@regione.toscana.it");

        javaMailSender.send(mimeMessage);
    }
}
