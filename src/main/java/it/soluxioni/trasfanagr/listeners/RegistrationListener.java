package it.soluxioni.trasfanagr.listeners;

import it.soluxioni.trasfanagr.events.OnRegistrationCompleteEvent;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.services.UtenteService;
import it.soluxioni.trasfanagr.utility.Costanti;
import it.soluxioni.trasfanagr.utility.MessaggiEmail;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private UtenteService utenteService;
    private JavaMailSender javaMailSender;

    public RegistrationListener(UtenteService utenteService, JavaMailSender javaMailSender) {
        this.utenteService = utenteService;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent onRegistrationCompleteEvent) {
        try {
            this.confermaRegistrazione(onRegistrationCompleteEvent);
        }catch (MessagingException e) {e.printStackTrace();}
    }


    private void confermaRegistrazione(OnRegistrationCompleteEvent event)  throws MessagingException{
        Utente utente = event.getUtente();
        HttpServletRequest request = event.getRequest();
        String url = request.getHeader("host");
        String token = UUID.randomUUID().toString();
        utenteService.creaTokenRegistrazione(utente, token);

        String email = utente.getEmail();

        String urlToken = Costanti.URL + "/utente/conferma/"+token;

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        String htmlMsg = MessaggiEmail.completataRegistrazioneMail(utente.getUsername(), urlToken);
        mimeMessage.setContent(htmlMsg, "text/html");
        helper.setTo(email);
        helper.setSubject("Sistema Informativo Anagrafe Regionale degli Studenti - Conferma Account");
        helper.setFrom("dati-studenti@regione.toscana.it");

        javaMailSender.send(mimeMessage);
    }
}
