package it.soluxioni.trasfanagr.listeners;

import it.soluxioni.trasfanagr.events.OnPasswordDimenticataEvent;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.utility.MessaggiEmail;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class PasswordDimenticataListener implements ApplicationListener<OnPasswordDimenticataEvent> {

    private JavaMailSender javaMailSender;

    public PasswordDimenticataListener(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnPasswordDimenticataEvent onPasswordDimenticataEvent)  {
        try{
            this.recuperaPassword(onPasswordDimenticataEvent);
        }catch (MessagingException e) {e.printStackTrace();}
    }

    private void recuperaPassword(OnPasswordDimenticataEvent onPasswordDimenticataEvent) throws MessagingException{
        Utente utente = onPasswordDimenticataEvent.getUtente();
        String password = onPasswordDimenticataEvent.getPassword();
        String email = utente.getEmail();

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        String htmlMsg = MessaggiEmail.inviaMail("Ciao utente,\n" + "Questa e' la tua nuova password, verrai invitato a modificarla al primo accesso <br> \n Password: "+password);
        mimeMessage.setContent(htmlMsg, "text/html");
        helper.setTo(email);
        helper.setSubject("Sistema Informativo Anagrafe Regionale degli Studenti - Recupero password");
        helper.setFrom("dati-studenti@regione.toscana.it");
        javaMailSender.send(mimeMessage);
    }
}
