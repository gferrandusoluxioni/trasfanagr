package it.soluxioni.trasfanagr.listeners;

import it.soluxioni.trasfanagr.events.OnDocumentsSentEvent;
import it.soluxioni.trasfanagr.model.Utente;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import static it.soluxioni.trasfanagr.utility.Costanti.APP_EMAIL;

@Component
public class DocumentsListener implements ApplicationListener<OnDocumentsSentEvent> {

    private JavaMailSender javaMailSender;

    public DocumentsListener(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnDocumentsSentEvent onDocumentsSentEvent) {
        this.documentiRicevuti(onDocumentsSentEvent);
    }

    private void documentiRicevuti(OnDocumentsSentEvent event) {
        Utente utente = event.getUtente();
        String username = utente.getUsername();

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(APP_EMAIL);
        message.setTo(APP_EMAIL);
        message.setSubject("Documenti Ricevuti da: " + username);
        message.setText( "Ciao ADMIN,\n" +
                "il Sig." +username+" ha appena inviato i suoi documenti.\n " +
                "\n Buona giornata" );
        javaMailSender.send(message);
    }
}
