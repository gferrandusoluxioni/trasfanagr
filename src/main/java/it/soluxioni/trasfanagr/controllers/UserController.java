package it.soluxioni.trasfanagr.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.soluxioni.trasfanagr.events.*;
import it.soluxioni.trasfanagr.exceptions.UtenteNonAutorizzatoException;
import it.soluxioni.trasfanagr.model.Caricamento;
import it.soluxioni.trasfanagr.model.TokenRegistrazione;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.repositories.IstituzioneRepository;
import it.soluxioni.trasfanagr.services.CaricamentoService;
import it.soluxioni.trasfanagr.services.UtenteService;
import it.soluxioni.trasfanagr.utility.Utility;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    private UserDetailsService userDetailsService;

    private IstituzioneRepository istituzioneRepository;
    private UtenteService utenteService;
    private ApplicationEventPublisher eventPublisher;
    private CaricamentoService caricamentoService;

    public UserController(UserDetailsService userDetailsService, IstituzioneRepository istituzioneRepository, UtenteService utenteService, ApplicationEventPublisher eventPublisher, CaricamentoService caricamentoService) {
        this.userDetailsService = userDetailsService;
        this.istituzioneRepository = istituzioneRepository;
        this.utenteService = utenteService;
        this.eventPublisher = eventPublisher;
        this.caricamentoService = caricamentoService;
    }

    @GetMapping("/registrazione")
    public String registrazioneForm(Model model, HttpServletRequest request) {
        model.addAttribute("utente", new Utente());
        return "registrazione";
    }

    @PostMapping("/registrazione")
    public String registrazioneSubmit(@ModelAttribute Utente utente) {
        return null;
    }

    @GetMapping("utente/conferma/{token}")
    public String confermaRegistrazione(@PathVariable("token") String token, HttpServletRequest request){
        TokenRegistrazione tokenRegistrazione = utenteService.getTokenRegistrazione(token);
        Map<String, String> risposta = new HashMap<>();
        if (tokenRegistrazione == null){
            risposta.put("esito", "ko");
            return "403";
        }
        Utente utente = tokenRegistrazione.getUser();
        Calendar cal = Calendar.getInstance();
        if ((tokenRegistrazione.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            risposta.put("esito", "exp");
            String tokenUtente = tokenRegistrazione.getToken();
            return "redirect:/indirizzo_scaduto?token="+tokenUtente;
        }
        String password = utenteService.salvaUtenteRegistrato(utente);
        eventPublisher.publishEvent(new OnRegistrationConfirmEvent(this, utente, password, request));
        risposta.put("esito", "ok");
        return "redirect:/indirizzo_verificato";
    }


    @PostMapping("indirizzo_scaduto/{token}")
    public String getCambioPasswordPage(@PathVariable("token") String token,  HttpServletRequest request) {
        TokenRegistrazione tokenRegistrazione = utenteService.getTokenRegistrazione(token);
        Utente utente = tokenRegistrazione.getUser();
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(this, utente, request));
        return "indirizzo_scaduto/"+token;
    }

    @GetMapping("/account")
    @PreAuthorize("hasRole('REGISTRATO')")
    public String getAccount(Model model, Principal principal, HttpServletRequest request) throws UtenteNonAutorizzatoException {
        String utenteRichiesto = request.getParameter("account");
        if(utenteRichiesto != null ){
            if (request.isUserInRole("ROLE_ADMIN")) {
                Utente utente = utenteService.getUtenteByUsername(utenteRichiesto);
                Utente richiedente = utenteService.getUtenteByUsername(principal.getName());
                if (utente != null) {
                    model.addAttribute("utente", utente);
                    model.addAttribute("richiedente", richiedente);
                } else {
                    //TODO errore
                }
            }

        } else{
            String username = principal.getName();
            Utente utente = utenteService.getUtenteByUsername(username);

            if(utente.isPrimaPasswordSiNo() == true){
                return "redirect:/cambio_prima_psw";
            }

            if(!utente.isCIdSiNo()){
                return "redirect:/allega_documento";
            }
            model.addAttribute("utente", utente);
            model.addAttribute("richiedente", utente);
        }
        return "account";
    }

    @PostMapping("/utente/cambio_psw" )
    @PreAuthorize("hasRole('REGISTRATO')")
    @ResponseBody
    public Map<String, String > getCambioPswPage(@RequestParam("param") String data, Principal principal) throws IOException, UtenteNonAutorizzatoException {
        Map<String, String> risposta = new HashMap<>();

        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        String messaggio = "";
        if(utente.isPrimaPasswordSiNo() == true) {

            messaggio = utenteService.cambiaPassword(data, principal.getName());

            switch (messaggio){
                case "VECCHIA": risposta.put("esito", "vecchia");
                            break;
                case "CRITERI": risposta.put("esito", "criteri");
                            break;
                case "OK": risposta.put("esito", "primo");
                            break;
                case "INESISTENTE": risposta.put("esito", "ko");
                            break;
            }

            return risposta;

        }

        messaggio = utenteService.cambiaPassword(data, principal.getName());

        switch (messaggio){
            case "VECCHIA": risposta.put("esito", "vecchia");
                break;
            case "CRITERI": risposta.put("esito", "criteri");
                break;
            case "OK": risposta.put("esito", "ok");
                break;
            case "INESISTENTE": risposta.put("esito", "ko");
                break;
            case "UGUALI": risposta.put("esito", "uguali");
                break;
        }
        return risposta;
    }

    @RequestMapping("/utente/elimina_documenti/{utente}")
    @PreAuthorize("hasRole('ADMIN')")
    public String eliminaDocumenti(@PathVariable("utente") String username, Model model) throws IOException {
        Utente utente = utenteService.getUtenteByUsername(username);
        Utility.eliminaFile(utente.getDocumentoCartaId());
        Utility.eliminaFile(utente.getDocumentoCodFisc());
        utente.setDocumentoCartaId(null);
        utente.setDocumentoCodFisc(null);
        utente.setCIdSiNo(false);
        utente.setCfSiNo(false);
        utente.setRuolo("REGISTRATO");
        utenteService.aggiornaUtente(utente);
        return "redirect:/lista_account";
    }

    @RequestMapping("/utente/abilita/{utente}")
    @PreAuthorize("hasRole('ADMIN')")
    public String abilitaUtente(@PathVariable("utente") String username){
        Utente utente = utenteService.getUtenteByUsername(username);
        if(utente.isCIdSiNo()){
            utente.setRuolo("ABILITATO");
            utenteService.aggiornaUtente(utente);
            eventPublisher.publishEvent(new OnUtenteAbilitatoEvent(this, utente));
        }
        return "redirect:/lista_account";
    }

    @RequestMapping("/utente/disabilita/{utente}")
    @PreAuthorize("hasRole('ADMIN')")
    public String disabilitaUtente(@PathVariable("utente") String username){
        Utente utente = utenteService.getUtenteByUsername(username);
        utente.setRuolo("REGISTRATO");
        utenteService.aggiornaUtente(utente);
        return "redirect:/lista_account";
    }

    @RequestMapping("/utente/elimina/{utente}")
    @PreAuthorize("hasRole('ADMIN')")
    public String eliminaUtente(@PathVariable("utente") String username) throws IOException{

        Utente utente = utenteService.getUtenteByUsername(username);
        List<Caricamento> caricamenti = utente.getCaricamenti();
        if(caricamenti != null){
            for(Caricamento upload : caricamenti){
                caricamentoService.eliminaCaricamentoDaDisco(upload.getId());
                caricamentoService.eliminaCaricamento(upload);
            }
            caricamenti = null;
        }

        utenteService.eliminaUtente(username);

        return "redirect:/lista_account";
    }

    @PostMapping("utente/recupera_username")
    @ResponseBody
    Map<String, String> recuperaUsername(@RequestParam("param") String data) throws IOException{

        Map<String, String> risposta = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(data);
        String codiceFiscale = jsonNode.get("codiceFiscale").toString().replace("\"", "").toUpperCase();
        Utente utente = utenteService.getUtenteByCodiceFiscale(codiceFiscale);
        if(utente != null){
            eventPublisher.publishEvent(new OnUsernameDimenticatoEvent(this, utente));
            risposta.put("esito", "ok");
            return risposta;
        }
        risposta.put("esito", "ko");
        return  risposta;
    }

    @RequestMapping("utente/recupera_psw/{username}")
    @ResponseBody
    Map<String, String> recuperaPassword(@PathVariable("username") String username) throws IOException{
        Map<String, String> risposta = new HashMap<>();

        String password = utenteService.rigeneraPasswordDimenticata(username);
        if(password != null){
            Utente utente = utenteService.getUtenteByUsername(username);
            eventPublisher.publishEvent(new OnPasswordDimenticataEvent(this, utente, password));
            risposta.put("esito", "ok");
            return risposta;
        }
        risposta.put("esito", "ko");
        return  risposta;
    }
}
