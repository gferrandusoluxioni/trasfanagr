package it.soluxioni.trasfanagr.controllers;

import it.soluxioni.trasfanagr.events.OnDocumentsSentEvent;
import it.soluxioni.trasfanagr.model.Caricamento;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.services.CaricamentoService;
import it.soluxioni.trasfanagr.services.UtenteService;
import it.soluxioni.trasfanagr.utility.Utility;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class FileController {

    private String basePath = "/trasf/hardDisk/";

    private UtenteService utenteService;
    private CaricamentoService caricamentoService;
    private ApplicationEventPublisher eventPublisher;

    public FileController(UtenteService utenteService, CaricamentoService caricamentoService, ApplicationEventPublisher eventPublisher) {
        this.utenteService = utenteService;
        this.caricamentoService = caricamentoService;
        this.eventPublisher = eventPublisher;
    }

    @PostMapping("carica_file")
    @PreAuthorize("hasRole('ABILITATO')")
    @ResponseBody
    public Map<String, String> caricaFiles(@RequestParam("files") MultipartFile[] files, @RequestParam("istituzioneCaricata") String istituzione, @RequestParam("note") String note, @RequestParam("tipoRilevazione") String tipoRilevazione, @RequestParam("anno") String anno, @RequestParam("singoloMultiplo") String singoloMultiplo, HttpServletRequest request, Principal principal) throws IOException {

            Map<String, String> risposta = new HashMap<>();

            String username = principal.getName();
            Utente utente = utenteService.getUtenteByUsername(username);
            if(username == null || username.isEmpty()){
                risposta.put("esito", "inesistente");
                return risposta;
            }

            String[] parts = istituzione.split(" - ");
            String istituzioneCaricata = parts[0];
            istituzioneCaricata=istituzioneCaricata.replace("\"", "");
            istituzioneCaricata=istituzioneCaricata.replace("[", "");
            istituzioneCaricata=istituzioneCaricata.replace("]", "");

            if( !((istituzioneCaricata != null && !istituzioneCaricata.equals("Seleziona l'istituzione per cui vuoi caricare i file ")) && (tipoRilevazione != null && !tipoRilevazione.equals("Seleziona tipo prelievo")) && (anno != null && !anno.equals("Seleziona anno scolastico")) && (singoloMultiplo != null && !singoloMultiplo.equals("Seleziona la tipologia di trasferimento")))) {

                risposta.put("esito", "campi");
                return risposta;
            }

            String[] estensioni = Utility.prendiEstensioni(files);

            boolean[] estensioniApprovate = Utility.controllaEstensioniCaricamento(estensioni, files.length);

            boolean tutteEstensioniOk = caricamentoService.controllaEstensioni(estensioni, estensioniApprovate);

            if(tutteEstensioniOk){

                String directoryName = basePath + username + "/caricamenti/" + new Date().toString().replace(" ","").trim();
                Utility.creaDirectory(directoryName);

                boolean isMultiple = singoloMultiplo.equals("2 O PIU' FILE") ? true : false;

                caricamentoService.salvaNuovoCaricamento(files, directoryName, estensioni, istituzioneCaricata, tipoRilevazione, anno, isMultiple, utente, note);
                risposta.put("esito", "ok");
                return risposta;

            } else {
                risposta.put("esito", "ext");
                    return risposta;
        }
    }

    @PostMapping("/allega_documento")
    @PreAuthorize("hasRole('REGISTRATO')")
    @ResponseBody
    public Map<String, String> submit(@RequestParam("files") MultipartFile[] files, ModelMap modelMap, HttpServletRequest request, Principal principal) throws IOException {
        Map<String, String> risposta = new HashMap<>();
        String username = principal.getName();
        if(username == null || username.isEmpty()){
            risposta.put("esito", "ko");
            return risposta;
        }

        String estensioneID = FilenameUtils.getExtension(files[0].getOriginalFilename());
        String estensioneCF = FilenameUtils.getExtension(files[1].getOriginalFilename());

        if (estensioneCF.isEmpty() || estensioneCF.isEmpty()){
            risposta.put("esito", "empty");
            return risposta;
        }

        boolean idOK = Utility.controllaEstensioneDocumento(estensioneID);
        boolean cfOK = Utility.controllaEstensioneDocumento(estensioneCF);

        if(!idOK || !cfOK ){
            risposta.put("esito", "ext");
            return risposta;
        }

        String directoryName = basePath + username + "/documenti";
        Utility.creaDirectory(directoryName);

        String idPath = directoryName+"/"+username+"-cartaId."+estensioneID;
        String cfPath = directoryName+"/"+username+"-codiceFisacle."+estensioneCF;

        if(!files[0].isEmpty())
            files[0].transferTo(new File(idPath));
        if(!files[1].isEmpty())
            files[1].transferTo(new File(cfPath));

        Utente user = utenteService.salvaDocumentiUtente(username, idPath, cfPath);
        if (user != null) {
            risposta.put("esito", "ok");
            eventPublisher.publishEvent(new OnDocumentsSentEvent(this, user));
        } else {
            risposta.put("esito", "inesistente");
        }
        return risposta;
    }

    @RequestMapping(path = "/documento/{utente}/{file}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public ResponseEntity<Resource> download(@PathVariable("file") String fileName, @PathVariable("utente") String username ) throws IOException {

        Utente utente = utenteService.getUtenteByUsername(username);
        File file;

        if(fileName.equals("id"))
            file = new File(utente.getDocumentoCartaId());
        else
            file = new File(utente.getDocumentoCodFisc());

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        String[] parts = file.getAbsolutePath().split("/");
        String nome = parts[parts.length-1];
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+nome);

        return ResponseEntity.ok()
                .contentLength(file.length())
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    @RequestMapping(path = "/caricamento/{utente}/{idCaricamento}/{file}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ABILITATO') or hasRole('PROVINCIA') or hasRole('ADMIN')")
    @ResponseBody
    public ResponseEntity<Resource> downloadCaricamento(@PathVariable("file") String fileName, @PathVariable("idCaricamento") String idCaricamento, @PathVariable("utente") String username, Principal principal ) throws IOException {

        //CASO utente chiede per se stesso
        if(principal.getName().equals(username)){
            Optional<Caricamento> caricamentoOpt = caricamentoService.prendiCaricamentoPerID(Long.valueOf(idCaricamento));
            Caricamento caricamento = caricamentoOpt.get();
            String percorso ;
            switch (fileName){
                case "file1":
                    percorso = caricamento.getUpload1();
                    break;
                case "file2":
                    percorso = caricamento.getUpload2();
                    break;
                case "file3":
                    percorso = caricamento.getUpload3();
                    break;
                case "file4":
                    percorso = caricamento.getUpload4();
                    break;
                case "file5":
                    percorso = caricamento.getUpload5();
                    break;
                case "file6":
                    percorso = caricamento.getUpload6();
                    break;
                case "file7":
                    percorso = caricamento.getUpload7();
                    break;
                case "file8":
                    percorso = caricamento.getUpload8();
                    break;
                default:
                    percorso = null;
            }

            if(percorso != null){
                File file = new File(percorso);
                Path path = Paths.get(file.getAbsolutePath());
                ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
                HttpHeaders headers = new HttpHeaders();
                String[] parts = percorso.split("/");
                String nome = parts[parts.length-1];

                headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+nome);
                return ResponseEntity.ok()
                        .contentLength(file.length())
                        .headers(headers)
                        .contentType(MediaType.parseMediaType("application/octet-stream"))
                        .body(resource);

            } else {
                return null;
            }
        }
        return null;
    }

    @RequestMapping("/caricamento/elimina/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String eliminaCaricamento(@PathVariable("id") String id) throws IOException{

        Optional<Caricamento> optionalCaricamento = caricamentoService.prendiCaricamentoPerID(Long.parseLong(id));
        Caricamento caricamento = optionalCaricamento.get();
        Utente utente = caricamento.getUtenteCaricante();
        caricamento.setUtenteCaricante(null);

        if(caricamento.getUpload1() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload1());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload2() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload2());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload3() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload3());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload4() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload4());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload5() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload5());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload6() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload6());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload7() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload7());
            Files.delete(fileToDeletePath);
        }
        if(caricamento.getUpload8() != null){
            Path fileToDeletePath = Paths.get(caricamento.getUpload8());
            Files.delete(fileToDeletePath);
        }

        List<Caricamento> caricamenti = utente.getCaricamenti();

        List<Caricamento> caricamentiRimanenti = caricamenti.stream()
                .filter( c -> !c.getId().toString().equals(id))
                .collect(Collectors.toList());

        utente.setCaricamenti(caricamentiRimanenti);
        caricamentoService.eliminaCaricamento(caricamento);
        utenteService.aggiornaUtente(utente);
        return "redirect:/lista_file";
    }
}
