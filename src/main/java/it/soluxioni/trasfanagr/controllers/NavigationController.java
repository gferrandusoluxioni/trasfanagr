package it.soluxioni.trasfanagr.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.soluxioni.trasfanagr.exceptions.UtenteNonAutorizzatoException;
import it.soluxioni.trasfanagr.model.Caricamento;
import it.soluxioni.trasfanagr.model.Istituzione;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.repositories.IstituzioneRepository;
import it.soluxioni.trasfanagr.services.CaricamentoService;
import it.soluxioni.trasfanagr.services.MailService;
import it.soluxioni.trasfanagr.services.UtenteService;
import it.soluxioni.trasfanagr.utility.Costanti;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class NavigationController extends BaseController{

    private UtenteService utenteService;
    private IstituzioneRepository istituzioneRepository;
    private CaricamentoService caricamentoService;

    private MailService mailService;

    public NavigationController(UtenteService utenteService, IstituzioneRepository istituzioneRepository, CaricamentoService caricamentoService,  MailService mailService) {
        this.utenteService = utenteService;
        this.istituzioneRepository = istituzioneRepository;
        this.caricamentoService = caricamentoService;

        this.mailService = mailService;
    }

    @GetMapping({"index"})
    @PreAuthorize("hasRole('REGISTRATO') or hasRole('ABILITATO') or hasRole('PROVINCIA') or hasRole('ADMIN')")
    public String getIndexPage(Principal principal, Model model){
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        if(utente.getRuolo().equals("REGISTRATO")){
            return "redirect:/account?tipo=tmp";
        }

        if(utente.isPrimaPasswordSiNo()){
            return "redirect:/cambio_prima_psw";
        }
        model.addAttribute("utente", utente);
        if(utente.getRuolo().equals("ADMIN")){
            List<Caricamento> caricamentiTotali = caricamentoService.prendiTutti();

            int numCaricamentiTot = caricamentiTotali.size();
            int numCaricamentiIntermedi = caricamentiTotali.stream()
                    .filter( c -> c.getTipoPrelievo().equals("INTERMEDIA"))
                    .collect(Collectors.toList()).size();
            int numCaricamentiFine = numCaricamentiTot - numCaricamentiIntermedi;

            model.addAttribute("totali", numCaricamentiTot);
            model.addAttribute("intermedi", numCaricamentiIntermedi);
            model.addAttribute("fine", numCaricamentiFine);
        }else{
            List<Caricamento> caricamenti = utente.getCaricamenti();

            int numCaricamentiTot = caricamenti.size();
            int numCaricamentiIntermedi = caricamenti.stream()
                    .filter( c -> c.getTipoPrelievo().equals("INTERMEDIA"))
                    .collect(Collectors.toList()).size();
            int numCaricamentiFine = numCaricamentiTot - numCaricamentiIntermedi;

            model.addAttribute("totali", numCaricamentiTot);
            model.addAttribute("intermedi", numCaricamentiIntermedi);
            model.addAttribute("fine", numCaricamentiFine);
        }
        return "index";
    }

    @RequestMapping("allega_documento")
    @PreAuthorize("hasRole('REGISTRATO')")
    public String getAllegaDocumentoPage(Principal principal, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Utente utente = utenteService.getUtenteByUsername(principal.getName());

        if(utente.isCfSiNo() && utente.isCIdSiNo()){
            return "redirect:index";
        }

        if(authentication.getAuthorities().size() > 1)
            return "redirect:index";

        model.addAttribute("utente", utente);

        return "allega_documento";
    }

    @GetMapping("carica_file")
    @PreAuthorize("hasRole('ABILITATO') or hasRole('PROVINCIA') or hasRole('ADMIN')")
    public String getCaricaFilePage(Principal principal, Model model, HttpServletRequest request){
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        model.addAttribute("utente", utente);
        return "carica_file";
    }

    @RequestMapping("lista_file")
    @PreAuthorize("hasRole('ABILITATO') or hasRole('PROVINCIA') or hasRole('ADMIN')")
    public String getListaFilePage(HttpServletRequest request, Principal principal, Model model) throws UtenteNonAutorizzatoException, IOException {
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        model.addAttribute("utente", utente);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
        listAuthorities.addAll(authorities);
            if(utente.getRuolo().equals("ADMIN")){
                List<Caricamento> caricamenti = caricamentoService.prendiTutti();
                model.addAttribute("caricamenti", caricamenti);
            } else {
                List<Caricamento> caricamenti = utente.getCaricamenti();
                model.addAttribute("caricamenti", utente.getCaricamenti());
            }

        return "lista_file";
    }


    @GetMapping("lista_account")
    @PreAuthorize("hasRole('ADMIN')")
    public String getAccountListPage(Model model, Principal principal){
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        List<Utente> utenti = utenteService.tuttiUtenti();
        model.addAttribute("utenti", utenti);
        model.addAttribute("utente", utente);
        return "lista_account";
    }


    @GetMapping("lista_istituzioni_non_accreditate")
    @PreAuthorize("hasRole('ADMIN')")
    public String getIstituzioniNonAssociateListPage(Model model, Principal principal){
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        Set<String> codiciIstituzioni = istituzioneRepository.findAllCodes();
        List<Istituzione> tutte = istituzioneRepository.findAll();
        Set<Istituzione> istituzioniNonAccreditate = new HashSet<>();
        for(int i=0; i<tutte.size(); i++){
            if(codiciIstituzioni.contains(tutte.get(i).getCodice()))
                istituzioniNonAccreditate.add(tutte.get(i));
        }
        model.addAttribute("istituzioni", istituzioniNonAccreditate);
        model.addAttribute("utente", utente);
        return "lista_istituzioni_non_accreditate";
    }

    @GetMapping("lista_istituzioni_no_archivi")
    @PreAuthorize("hasRole('ADMIN')")
    public String getIstituzioniNoArchiviPage(Model model, Principal principal){
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        List<Istituzione> istituzioniAssociate = istituzioneRepository.findByResponsabileIsNotNull();
        Set<Istituzione> istituzioniNonCaricanti = new HashSet<>();
        for(Istituzione istituzione:istituzioniAssociate){
            if(istituzione.getResponsabile().getCaricamenti().size() == 0) {
                istituzioniNonCaricanti.add(istituzione);
            }
        }

        model.addAttribute("istituzioni", istituzioniNonCaricanti);
        model.addAttribute("utente", utente);
        return "lista_istituzioni_no_archivi";
    }

    @GetMapping("privacy")
    public String getPrivacyPage(){

        return "privacy";
    }


    @GetMapping({"", "/", "/login"})
    public String getLoginPage(Principal principal, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<GrantedAuthority> listAuthorities = new ArrayList<GrantedAuthority>();
        listAuthorities.addAll(authorities);

        for (GrantedAuthority role : listAuthorities) {
            String ruolo = role.getAuthority().toString();
            if(ruolo.equals("ROLE_ANONYMOUS")){
                return "login";
            }
        }
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        if(utente.getRuolo().equals("REGISTRATO")){
            return "redirect:/account?tipo=tmp";
        }

        return "redirect:index";
    }

    @RequestMapping(method=GET, value={"indirizzo_scaduto", "indirizzo_scaduto/{token}"})
    public String getIndirizzoScadutoPage() {
        return "indirizzo_scaduto"; }

    @RequestMapping("indirizzo_verificato")
    public String getIndirizzoVerificatoPage() { return "indirizzo_verificato"; }

    @GetMapping("recupera_psw")
    public String getRecuperaPasswordPage() { return "recupera_psw"; }

    @GetMapping("cambio_prima_psw")
    @PreAuthorize("hasRole('REGISTRATO')")
    public String getCambioPrimaPasswordPage(Principal principal, Model model) {
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        model.addAttribute("utente", utente);
        return "cambio_prima_psw"; }

    @GetMapping("cambio_psw")
    public String getCambioPasswordPage(Principal principal, Model model) {
        Utente utente = utenteService.getUtenteByUsername(principal.getName());
        model.addAttribute("utente", utente);
        return "cambio_psw"; }

    @GetMapping("invia_mail_utente")
    @PreAuthorize("hasRole('ADMIN')")
    public String getInviaPage(HttpServletRequest request, Model model) {
        String email = request.getParameter("mail");
        String mittente = Costanti.APP_EMAIL;
        model.addAttribute("mittente", mittente);
        model.addAttribute("email", email);
        return "invia_mail_utente";
    }


    @PostMapping("invia_mail_utente")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public Map<String, String> postInviaPage(@RequestParam("param") String jsonData, HttpServletRequest request, Model model) throws MessagingException, IOException {
        Map<String, String> risposta = new HashMap<>();

        String data = jsonData;

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(data);
        String messaggio = jsonNode.get("testo").toString().replace("\"", "");
        String oggetto = jsonNode.get("oggetto").toString().replace("\"", "");
        String indirizzo = jsonNode.get("indirizzo").toString().replace("\"", "");
        if( (messaggio != null && !messaggio.isEmpty()) && (oggetto != null && !oggetto.isEmpty())){
            try{
                mailService.inviaMail(oggetto, messaggio, indirizzo);
                risposta.put("esito", "successo");
                return  risposta;
           } catch (Exception e) {
               risposta.put("esito", "errore");
               return risposta;
           }
        } else{
            risposta.put("esito", "warning");
            return  risposta;
        }

    }

    @GetMapping("recupera_user")
    public String getRecuperaUserPage(){
        return "recupera_user";
    }

    @RequestMapping("403")
    public String get403Page() {
        return "403";
    }

}
