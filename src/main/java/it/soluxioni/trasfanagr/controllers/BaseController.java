package it.soluxioni.trasfanagr.controllers;

import it.soluxioni.trasfanagr.exceptions.UtenteNonAutorizzatoException;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class BaseController {

    @ExceptionHandler( UtenteNonAutorizzatoException.class)
    public String handleException() {
        return "offline";
    }

}
