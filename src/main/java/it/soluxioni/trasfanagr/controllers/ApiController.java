package it.soluxioni.trasfanagr.controllers;

import it.soluxioni.trasfanagr.events.OnRegistrationCompleteEvent;
import it.soluxioni.trasfanagr.model.Istituzione;
import it.soluxioni.trasfanagr.model.Utente;
import it.soluxioni.trasfanagr.repositories.IstituzioneRepository;
import it.soluxioni.trasfanagr.services.GeoService;
import it.soluxioni.trasfanagr.services.UtenteService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
public class ApiController {

    private UtenteService utenteService;
    private GeoService geoService;
    private IstituzioneRepository istituzioneRepository;

    private ApplicationEventPublisher eventPublisher;

    public ApiController(UtenteService utenteService, GeoService geoService, IstituzioneRepository istituzioneRepository, ApplicationEventPublisher eventPublisher) {
        this.utenteService = utenteService;
        this.geoService = geoService;
        this.istituzioneRepository = istituzioneRepository;

        this.eventPublisher = eventPublisher;
    }

    @GetMapping("/api/regioni/{regione}")
    Map<String, String> getProvince(@PathVariable("regione") String codRegione ){
        return geoService.getProvince(codRegione);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/api/nazioni")
    Map<String, String> getNazioni(){
        return geoService.getNazioni();
    }

    @GetMapping("/api/regioni")
    Map<String, String> getRegioniItaliane(){
        return geoService.getRegionitaliane();
    }

    @GetMapping("/api/regioni/{regione}/{provincia}")
    Map<String, String> getComuni(@PathVariable("regione") String codRegione, @PathVariable("provincia") String codProvincia){
        return geoService.getComuni(codProvincia);
    }

    @GetMapping("/api/istituzioni/{codice}")
    Map<String, String> getIstituzione(@PathVariable("codice") String codice) {

        Istituzione istituzione = istituzioneRepository.findByCodice(codice);
        Map<String, String> codiceNomeIstituzione = new HashMap<>();
        codiceNomeIstituzione.put(istituzione.getCodice(), istituzione.getDenominazione());
        return codiceNomeIstituzione;
    }

    @GetMapping("api/istituzioni/codici")
    Set<String> getCodiciIstituzioni(){
        Set<String> codici = istituzioneRepository.findAllCodes();
        return codici;
    }



    @CrossOrigin(origins = "*")
    @PostMapping("api/utente/nuovo")
    void registraUtente(@RequestParam(value = "param") String user, HttpServletRequest request) throws IOException, ParseException {

        Utente nuovoAccount = utenteService.registraNuovoAccount(user);
        if(nuovoAccount != null)
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(this, nuovoAccount, request));

    }

    @PostMapping("/api/codiceFiscale/{codiceFiscale}")
    Map<String, String> controllaCodiceFiscale(@PathVariable("codiceFiscale") String codiceFiscale, HttpServletRequest request) throws IOException, ParseException {
        Utente utente= utenteService.getUtenteByCodiceFiscale(codiceFiscale.toUpperCase());
        Map<String, String> risposta = new HashMap<>();
        if(utente == null){
            risposta.put("esito", "ok");
            return risposta;
        } else{
            risposta.put("esito", "ko");
            return risposta;
        }
    }

    @PostMapping("/api/email/{email}")
    Map<String, String> controllaEmail(@PathVariable("email") String email) throws IOException, ParseException {
        Utente utente= utenteService.getUtenteByEmail(email.toUpperCase());
        Map<String, String> risposta = new HashMap<>();
        if(utente == null){
            risposta.put("esito", "ok");
            return risposta;
        } else{
            risposta.put("esito", "ko");
            return risposta;
        }
    }
}
