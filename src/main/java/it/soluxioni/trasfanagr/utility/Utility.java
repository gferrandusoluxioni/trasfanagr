package it.soluxioni.trasfanagr.utility;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

public class Utility {

    private final static Set<String> estensioniConsentiteDocumento = new HashSet<>(Arrays.asList("jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "pdf", "PDF"));

    private final static Set<String> estensioniConsentiteCaricamento = new HashSet<>(Arrays.asList("rar", "RAR", "zip", "ZIP", "xls", "XLS", "xlsx", "XLSX", "csv", "CSV", "txt", "TXT", "7z", "7Z", "tar", "TAR", "gz", "GZ"));

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public static boolean controllaEstensioneDocumento(String estensione){
        if(estensioniConsentiteDocumento.contains(estensione) )
            return true;
        return false;
    }

    public static boolean controllaEstensioneCaricamento(String estensione){
        if(estensioniConsentiteCaricamento.contains(estensione) || estensione.equals(""))
            return true;
        return false;
    }

    public static void eliminaFile(String fileName) throws IOException {
        Path fileToDeletePath = Paths.get(fileName);
        Files.delete(fileToDeletePath);
    }

    public static boolean checkCriteriPassword(String input){
        String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        if (Pattern.matches(regex, input))
            return true;
        else
            return false;
    }

    public static void creaDirectory(String path){
        File directory = new File(path);
        if (! directory.exists()){
            directory.mkdirs();
        }
    }

    public static String[] prendiEstensioni(MultipartFile[] files){
        String[] estensioni = new String[8];
        for(int i = 0; i< files.length; i++){
            estensioni[i]= FilenameUtils.getExtension(files[i].getOriginalFilename());
        }
        return estensioni;
    }

    public static boolean[] controllaEstensioniCaricamento(String[] estensioni, int dimensione){
        boolean[] estensioniOK = new boolean[dimensione];
        for(int i = 0; i< dimensione; i++){
            estensioniOK[i] = Utility.controllaEstensioneCaricamento(estensioni[i]);
        }
        return estensioniOK;
    }

    /*public static Istituzione convertiIstituzionePreToIstituzione(IstituzionePre istituzionePre ,Istituzione istituzione){

        istituzione.setResponsabile(istituzionePre.getResponsabile());
        istituzione.setCap(istituzionePre.getCap());
        istituzione.setCodice(istituzionePre.getCodice());
        istituzione.setCodiceComune(istituzionePre.getCodiceComune());
        istituzione.setDenominazione(istituzionePre.getDenominazione());
        istituzione.setDescrizioneTipologiaGradoIstruzione(istituzionePre.getDescrizioneTipologiaGradoIstruzione());
        istituzione.setEmail(istituzionePre.getEmail());
        istituzione.setIndirizzo(istituzionePre.getIndirizzo());
        istituzione.setNomeComune(istituzionePre.getNomeComune());
        istituzione.setPec(istituzionePre.getPec());
        istituzione.setProvincia(istituzionePre);
    }*/
}
