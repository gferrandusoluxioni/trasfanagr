package it.soluxioni.trasfanagr.utility;

public class Costanti {

    public static final String URL = "https://www.osservatorioscolastico.regione.toscana.it";
    public static final String APP_EMAIL = "dati-studenti@regione.toscana.it";
    public static final int SCADENZA_LINK = 60 * 1 ;
}
